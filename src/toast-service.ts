export class ToastService {
  counter: number = 0
  toasts: IToast[] = []

  toast(msg: string) {
    const id = this.counter++
    this.toasts.push({
      id,
      msg,
    })
    window.setTimeout(() => {
      const idx = this.toasts.findIndex(t => t.id === id)
      this.toasts.splice(idx, 1)
    }, 3_000)
  }
}

interface IToast {
  id: number
  msg: string
}
