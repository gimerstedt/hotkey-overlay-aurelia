import { autoinject } from 'aurelia-framework'
import { ce } from 'ce'
import { Hotkey } from 'hotkey'
import { HotkeyOverlay } from 'hotkey-overlay'
import { ToastService } from 'toast-service'
import { Toaster } from 'toaster'
import template from './app.html'

@ce({
  name: 'app',
  template,
  dependencies: [Hotkey, HotkeyOverlay, Toaster],
})
@autoinject
export class App {
  constructor(private toastService: ToastService) {}

  toast(k: string) {
    this.toastService.toast(k+' clicked')
  }
}
