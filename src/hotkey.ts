import { customAttribute } from 'aurelia-framework'
import { HotKeyState } from 'hotkey-state'

@customAttribute('hotkey')
export class Hotkey {
  private value: string
  private key: string

  constructor(private element: Element, private state: HotKeyState) {}

  bind() {
    this.key = this.state.addElement(this.element, this.value)
  }

  unbind() {
    this.state.removeElement(this.key)
  }
}
