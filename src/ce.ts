interface ICEConfig {
  name: string
  template: string | HTMLTemplateElement
  dependencies?:
    | Function[]
    | { (): (Promise<Record<string, Function>> | Function)[] }
}

export function ce({ template, dependencies, name }: ICEConfig): any {
  return function(target) {
    target.$view = {
      template,
      dependencies,
    }
    target.$resource = { name }
  }
}
