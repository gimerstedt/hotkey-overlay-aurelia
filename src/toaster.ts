import { autoinject } from 'aurelia-framework'
import { ce } from 'ce'
import { ToastService } from 'toast-service'
import template from './toaster.html'

@ce({
  name: 'toast',
  template,
})
@autoinject
export class Toaster {
  toasts

  constructor(private toastService: ToastService) {}

  bind() {
    this.toasts = this.toastService.toasts
  }
}
