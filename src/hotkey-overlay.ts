import { autoinject } from 'aurelia-framework'
import { ce } from 'ce'
import { HotKeyState } from 'hotkey-state'
import template from './hotkey-overlay.html'

@ce({
  name: 'hotkey-overlay',
  template,
})
@autoinject
export class HotkeyOverlay {
  constructor(private state: HotKeyState) {}

  attached() {
    document.addEventListener('keydown', this.handler)
  }

  detached() {
    document.removeEventListener('keydown', this.handler)
  }

  handler = ({ key }) => {
    if (key === 'f')
      if (this.state.active) {
        this.state.active = false
        return
      } else {
        this.state.active = true
        this.generateStyles()
        return
      }
    if (!this.state.active) return
    this.click(key)
  }

  generateStyles() {
    this.state.generateStyles()
  }

  click(key: string) {
    this.state.active = false
    try {
      const hk = this.state.hotkeys.find(h => h.key === key)
      if (!hk) return
      //@ts-ignore
      hk.element.click()
    } catch (e) {
      console.log('caught in click:', e)
    }
  }
}
