const ALL = '1.2.3.4.5.6.7.8.9.0.q.w.e.r.t.y.u.i.o.p.a.s.d.g.h.j.k.l.z.x.c.v.b.n.m'.split(
  '.',
)
export class HotKeyState {
  private inUse: string[] = []

  public hotkeys: IHotkey[] = []
  public active: boolean

  addElement(element: Element, suggestion?: string): string {
    if (suggestion && suggestion.length !== 1) throw Error('one key please')
    const key = this.getKey(suggestion)
    this.hotkeys.push({
      key,
      element,
    })
    return key
  }

  removeElement(key: string) {
    const idx1 = this.hotkeys.findIndex(hk => hk.key === key)
    this.hotkeys.splice(idx1, 1)
    const idx2 = this.inUse.indexOf(key)
    this.inUse.splice(idx2, 1)
  }

  generateStyles() {
    for (const hk of this.hotkeys) {
      const { x, y } = getElementPos(hk.element)
      hk.style = {
        'background-color': '#afa',
        'font-family': 'fira-code',
        'font-size': '1em',
        border: '1px solid black',
        opacity: 0.8,
        padding: '3px',
        position: 'absolute',
        left: x + 'px',
        top: y + 'px',
      }
    }
  }

  private getKey(suggestion?: string): string {
    if (suggestion && !this.inUse.includes(suggestion)) {
      this.inUse.push(suggestion)
      return suggestion
    }
    const key = this.figureItOut()
    this.inUse.push(key)
    return key
  }

  private figureItOut(): string {
    return ALL.filter(k => this.inUse.indexOf(k) < 0)[0]
  }
}

interface IHotkeyStyle {}

interface IHotkey {
  key: string
  element: Element
  style?: IHotkeyStyle
}

const getElementPos = (el: Element) => {
  const r = el.getBoundingClientRect()
  return {
    x: r.left + window.scrollX,
    y: r.top + window.scrollY,
  }
}
